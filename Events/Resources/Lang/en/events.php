<?php
return [
    'list' => 'Events list',
    'search' => 'Enter the title of the event',
    'sort_title' => 'Title',
    'sort_published' => 'Published date',
    'sort_updated' => 'Update date',
    'edit' => 'Edit',
    'blank' => 'Open in new window',
    'empty' => 'List is empty',
    'delete' => [
        'question' => 'Delete event',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding event',
        'form' => [
            'title' => 'Title',
            'tags' => 'Tags',
            'tags_help_description' => 'List separated by commas',
            'announce' => 'Announce',
            'content' => 'Content',
            'main_image' => 'Main picture',
            'main_image_help_description' => 'Supported formats: png, jpg, gif',
            'announce_image' => 'Image of announcement',
            'announce_image_help_description' => 'Supported formats: png, jpg, gif',
            'image_select' => 'Select',
            'image_change' => 'Change',
            'image_delete' => 'Delete',
            'gallery' => 'Gallery',
            'publish' => 'Publish',
            'published_start' => 'The event start',
            'published_stop' => 'End of the event',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit event',
        'form' => [
            'title' => 'Title',
            'tags' => 'Tags',
            'tags_help_description' => 'List separated by commas',
            'announce' => 'Announce',
            'content' => 'Content',
            'main_image' => 'Main picture',
            'main_image_help_description' => 'Supported formats: png, jpg, gif',
            'announce_image' => 'Image of announcement',
            'announce_image_help_description' => 'Supported formats: png, jpg, gif',
            'image_select' => 'Select',
            'image_change' => 'Change',
            'image_delete' => 'Delete',
            'gallery' => 'Gallery',
            'publish' => 'Publish',
            'published_start' => 'The event start',
            'published_stop' => 'End of the event',
            'submit' => 'Save'
        ]
    ]
];