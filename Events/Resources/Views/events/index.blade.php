@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="active">
            <i class="{{ config('solutions_events::menu.icon') }}"></i> {!! array_translate(config('solutions_events::menu.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_events::menu.icon') }}"></i>
            {!! array_translate(config('solutions_events::menu.title')) !!}
        </h2>
    </div>
    @if(\PermissionsController::allowPermission('solutions_events', 'create', FALSE))
        @BtnAdd('solutions.events.create')
    @endif
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('solutions_events_lang::events.list')
                </div>
                @if($events->count())
                    {!! Form::open(['route' => 'solutions.events.index', 'method' => 'get']) !!}
                    <div class="ah-search">
                        <input type="text" name="search" placeholder="@lang('solutions_events_lang::events.search')"
                               class="ahs-input">
                        <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                    </div>
                    {!! Form::close() !!}
                    <ul class="actions">
                        <li>
                            <a href="" data-ma-action="action-header-open">
                                <i class="zmdi zmdi-search"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-asc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right z-index-max">
                                <li>
                                    <a href="{{ route('solutions.events.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'asc'])) }}">
                                        @lang('solutions_events_lang::events.sort_title')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.events.index', array_merge(Request::all(), ['sort_field' => 'published_start', 'sort_direction' => 'asc'])) }}">
                                        @lang('solutions_events_lang::events.sort_published')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.events.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'asc'])) }}">
                                        @lang('solutions_events_lang::events.sort_updated')
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-desc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{{ route('solutions.events.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'desc'])) }}">
                                        @lang('solutions_events_lang::events.sort_title')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.events.index', array_merge(Request::all(), ['sort_field' => 'published_start', 'sort_direction' => 'desc'])) }}">
                                        @lang('solutions_events_lang::events.sort_published')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.events.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'desc'])) }}">
                                        @lang('solutions_events_lang::events.sort_updated')
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                @endif
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($events as $events_single)
                    <div class="js-item-container list-group-item media">
                        @if($events_single->announce_image && \Storage::exists($events_single->announce_image))
                            <div class="pull-left clearfix">
                                <img alt="{{ $events_single->title }}" class="lv-img h-50 brd-0"
                                     src="{{ asset('uploads/' . $events_single->announce_image) }}">
                            </div>
                        @endif
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    @if(\PermissionsController::allowPermission('solutions_events', 'edit', FALSE))
                                        <li>
                                            <a href="{{ route('solutions.events.edit', $events_single->id) }}">@lang('solutions_events_lang::events.edit')</a>
                                        </li>
                                    @endif
                                    @if($events_single->publication)
                                        <li>
                                            <a href="{{ route('public.events.show', $events_single->PageUrl) }}"
                                               target="_blank">
                                                @lang('solutions_events_lang::events.blank')
                                            </a>
                                        </li>
                                    @endif
                                    @if(\PermissionsController::allowPermission('solutions_events', 'delete', FALSE))
                                        <li class="divider"></li>
                                        <li>
                                            <a class="c-red js-item-remove" href="">
                                                @lang('solutions_events_lang::events.delete.submit')
                                            </a>
                                            {!! Form::open(['route' => ['solutions.events.destroy', $events_single->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                            <button type="submit"
                                                    data-question="@lang('solutions_events_lang::events.delete.question') &laquo;{{ $events_single->title }}&raquo;?"
                                                    data-confirmbuttontext="@lang('solutions_events_lang::events.delete.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('solutions_events_lang::events.delete.cancelbuttontext')">
                                                @lang('solutions_events_lang::events.delete.submit')
                                            </button>
                                            {!! Form::close() !!}
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">{{ $events_single->title }}</div>
                            <small class="c-grey f-10">
                                {{ $events_single->announce }}
                            </small>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('solutions_events_lang::events.empty')</h2>
                @endforelse
            </div>
            <div class="lg-pagination p-10">
                {!! $events->appends(\Request::only(['search', 'sort_field', 'sort_direction']))->render() !!}
            </div>
        </div>
    </div>
@stop