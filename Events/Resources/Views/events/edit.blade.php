@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="{{ route('solutions.events.index') }}">
                <i class="{{ config('solutions_events::menu.icon') }}"></i> {!! array_translate(config('solutions_events::menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('solutions_events_lang::events.replace.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-edit"></i> @lang('solutions_events_lang::events.replace.title')</h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::model($event, ['route' => ['solutions.events.update', $event->id], 'class' => 'form-validate', 'id' => 'edit-solutions-events-form', 'method' => 'PUT', 'files' => TRUE]) !!}
                <div class="col-sm-10">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('solutions_events_lang::events.replace.form.title')</label>
                        </div>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::textarea('announce', NULL, ['class' => 'form-control auto-size fg-input', 'data-autosize-on' => 'true', 'rows' => 1]) !!}
                            <label class="fg-label">@lang('solutions_events_lang::events.replace.form.announce')</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="c-gray m-b-20">@lang('solutions_events_lang::events.replace.form.content')</p>
                        {!! Form::textarea('content', NULL, ['id' => 'event_content']) !!}
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <p class="c-gray m-b-5">@lang('solutions_events_lang::events.replace.form.main_image')</p>
                            <small class="help-description">@lang('solutions_events_lang::events.replace.form.main_image_help_description')</small>
                            <div class="fileinput  m-t-5{!! $event->main_image ? ' fileinput-exists' : ' fileinput-new' !!}"
                                 data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                     style="line-height: 150px;">
                                    @if($event->main_image)
                                        <img src="{!! asset('uploads' . $event->main_image)!!}">
                                    @endif
                                </div>
                                <div>
                                    <div class="btn btn-info btn-file">
                                        <span class="fileinput-new">@lang('solutions_events_lang::events.replace.form.image_select')</span>
                                        <span class="fileinput-exists">@lang('solutions_events_lang::events.replace.form.image_change')</span>
                                        {!! Form::file('main_image', ['class' => 'image-file']) !!}
                                        {!! Form::hidden('main_image_delete', 0, ['class' => 'image-delete', 'autocomplete' => 'off']) !!}
                                    </div>
                                    <a href="#" class="btn btn-danger fileinput-exists"
                                       data-dismiss="fileinput">@lang('solutions_events_lang::events.replace.form.image_delete')</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <p class="c-gray m-b-5">@lang('solutions_events_lang::events.replace.form.announce_image')</p>
                            <small class="help-description">@lang('solutions_events_lang::events.replace.form.announce_image_help_description')</small>
                            <div class="fileinput m-t-5{!! $event->announce_image ? ' fileinput-exists' : ' fileinput-new' !!}" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                     style="line-height: 150px;">
                                    @if($event->announce_image)
                                        <img src="{!! asset('uploads' . $event->announce_image) !!}">
                                    @endif
                                </div>
                                <div>
                                    <div class="btn btn-info btn-file">
                                        <span class="fileinput-new">@lang('solutions_events_lang::events.replace.form.image_select')</span>
                                        <span class="fileinput-exists">@lang('solutions_events_lang::events.replace.form.image_change')</span>
                                        {!! Form::file('announce_image', ['class' => 'image-file']) !!}
                                        {!! Form::hidden('announce_image_delete', 0, ['class' => 'image-delete', 'autocomplete' => 'off']) !!}
                                    </div>
                                    <a href="#" class="btn btn-danger fileinput-exists"
                                       data-dismiss="fileinput">@lang('solutions_events_lang::events.replace.form.image_delete')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(\PermissionsController::isPackageEnabled('core_galleries'))
                        @set($galleries_list, \STALKER_CMS\Core\Galleries\Models\Gallery::pluck('title', 'id'))
                        @if(count($galleries_list))
                            @set($galleries, ['' => NULL])
                            @foreach($galleries_list as $gallery_id => $gallery_title)
                                @set($galleries[$gallery_id], $gallery_title)
                            @endforeach
                            <div class="form-group">
                                <p class="c-gray m-t-10 m-b-10">@lang('solutions_events_lang::events.replace.form.gallery')</p>
                                {!! Form::select('gallery_id', $galleries, NULL, ['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                            </div>
                        @else
                            <div class="form-group">
                                {!! Form::hidden('gallery_id') !!}
                            </div>
                        @endif
                    @endif
                    <div class="form-group">
                        <p class="c-gray m-b-20">@lang('solutions_events_lang::events.replace.form.tags')</p>
                        {!! Form::text('tags', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        <small class="help-description">@lang('solutions_events_lang::events.replace.form.tags_help_description')</small>
                    </div>
                    @if(\PermissionsController::isPackageEnabled('core_seo') && \PermissionsController::allowPermission('solutions_events', 'seo', FALSE))
                        @include('core_seo_views::seo', ['seo' => $event])
                    @endif
                    @if(\PermissionsController::isPackageEnabled('core_open_graph') && \PermissionsController::allowPermission('solutions_events', 'open_graph', FALSE))
                        @include('core_open_graph_views::open_graph', ['open_graph' => $event])
                    @endif
                </div>
                <div class="col-sm-2">
                    @if(\App::getLocale() == 'ru')
                        @set($format, 'd.m.Y H:i')
                    @else
                        @set($format, 'm/d/Y H:i')
                    @endif
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('published_start', !empty($event->published_start) ? $event->published_start->format($format) : NULL, ['class' => 'input-sm form-control fg-input date-time-picker date-time-mask text-center']) !!}
                            <label class="fg-label">@lang('solutions_events_lang::events.replace.form.published_start')</label>
                        </div>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('published_stop', !empty($event->published_stop) ? $event->published_stop->format($format) : NULL, ['class' => 'input-sm form-control fg-input date-time-picker date-time-mask text-center']) !!}
                            <label class="fg-label">@lang('solutions_events_lang::events.replace.form.published_stop')</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('publication', TRUE, FALSE, ['autocomplete' => 'off']) !!}
                                <i class="input-helper"></i> @lang('solutions_events_lang::events.replace.form.publish')
                            </label>
                        </div>
                    </div>
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('solutions_events_lang::events.replace.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    @set($summernote_locale, \App::getLocale() . '-' . strtoupper(\App::getLocale()))
    {!! Html::script('core/summernote/summernote-' . $summernote_locale . '.js') !!}
    <script>
        $(".fileinput-exists .btn-danger").click(function () {
            $(this).siblings('.btn-file').find('.image-delete').val(1);
        });
        $(document).on('click', '.fileinput-new .image-file', function () {
            $(this).siblings('.image-delete').val(0);
        });
        $("#event_content").summernote({
            height: 250,
            tabsize: 2,
            lang: '{{ $summernote_locale }}',
            toolbar: [
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture']],
                ['view', ['fullscreen', 'codeview']],
            ]
        });
        $('#event_content').on('summernote.change', function (we, contents, $editable) {
            $("#event_content").html(contents);
            $("#event_content").change();
        });
        $("#edit-solutions-events-form input[name='tags']").SelectizeInput();
        var form = $("#edit-solutions-events-form");
        BASIC.currentForm = form;
        BASIC.validateOptions.rules = {
            title: {required: true},
            published_start: {required: true},
            published_stop: {required: true}
        };
        BASIC.validateOptions.messages = VALIDATION_MESSAGES.defaulRules;
        $(BASIC.currentForm).validate(BASIC.validateOptions);

        if ($("#seo-container").length) {
            $(document).on('click', '#seo-container .auto-paste', function () {
                var title = $(form).find('input[name="title"]').val().trim();
                $(form).find('input[name="seo_url"]').val(BASIC.transliterate(title)).focus();
                $(form).find('input[name="seo_title"]').val(title).focus();
                $(form).find('textarea[name="seo_description"]').val($(form).find('textarea[name="announce"]').val().trim()).focus();
                $(form).find('input[name="seo_h1"]').val(title).focus();
            })
        }
        if ($("#open-graph-container").length) {
            $(document).on('click', '#open-graph-container .auto-paste', function () {
                $(form).find('input[name="og[og:title]"]').val($(form).find('input[name="title"]').val().trim()).focus();
                $(form).find('textarea[name="og[og:description]"]').val($(form).find('textarea[name="announce"]').val().trim()).focus();
            })
        }
    </script>
@stop