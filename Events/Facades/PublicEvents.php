<?php
namespace STALKER_CMS\Solutions\Events\Facades;

use Illuminate\Support\Facades\Facade;

class PublicEvents extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicEventsController';
    }
}