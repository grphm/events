<?php
namespace STALKER_CMS\Solutions\Events\Providers;

use STALKER_CMS\Solutions\Events\Http\Controllers\PublicEventsController;
use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

class ModuleServiceProvider extends BaseServiceProvider {

    public function boot() {

        $this->setPath(__DIR__.'/../');
        $this->registerViews('solutions_events_views');
        $this->registerLocalization('solutions_events_lang');
        $this->registerConfig('solutions_events::config', 'Config/events.php');
        $this->registerSettings('solutions_events::settings', 'Config/settings.php');
        $this->registerActions('solutions_events::actions', 'Config/actions.php');
        $this->registerSystemMenu('solutions_events::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
        $this->publishesTemplates();
    }

    public function register() {

        \App::bind('PublicEventsController', function() {

            return new PublicEventsController();
        });
    }

    /********************************************************************************************************************/
    protected function registerBladeDirectives() {
    }

    public function publishesTemplates() {

        $this->publishes([
            __DIR__.'/../Resources/Templates' => base_path('home/Resources')
        ]);
    }
}
