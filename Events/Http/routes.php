<?php
\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function() {

    \Route::resource('events', 'EventsController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'solutions.events.index',
                'create' => 'solutions.events.create',
                'store' => 'solutions.events.store',
                'edit' => 'solutions.events.edit',
                'update' => 'solutions.events.update',
                'destroy' => 'solutions.events.destroy'
            ]
        ]
    );
});
$events_segment = settings(['solutions_events', 'events', 'first_segment']);
\Route::group(['prefix' => empty($events_segment) ? 'events' : $events_segment, 'middleware' => 'public'], function() {

    Route::get('{event_url}', ['as' => 'public.events.show', function($event_url) {

        return \PublicEvents::showEventPage($event_url);
    }]);
});