<?php
namespace STALKER_CMS\Solutions\Events\Http\Controllers;

use STALKER_CMS\Solutions\Events\Models\Event;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

class EventsController extends ModuleController implements CrudInterface {

    protected $model;

    public function __construct(Event $event) {

        $this->model = $event;
        $this->middleware('auth');
    }

    public function index() {

        \PermissionsController::allowPermission('solutions_events', 'events');
        $request = \RequestController::init();
        $events = $this->model;
        if($request::has('sort_field') && $request::has('sort_direction')):
            foreach(explode(', ', $request::get('sort_field')) as $index):
                $events = $events->orderBy($index, $request::get('sort_direction'));
            endforeach;
        endif;
        if($request::has('search')):
            $search = $request::get('search');
            $events = $events->where(function($query) use ($search) {

                $query->where('title', 'like', '%'.$search.'%');
            });
        endif;
        $events = $events->orderBy('created_at', 'DESC')->paginate(10);
        return view('solutions_events_views::events.index', compact('events'));
    }

    public function create() {

        \PermissionsController::allowPermission('solutions_events', 'create');
        return view('solutions_events_views::events.create');
    }

    public function store() {

        \PermissionsController::allowPermission('solutions_events', 'create');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            if(\PermissionsController::isPackageEnabled('core_seo')):
                $this->model->forbiddenURL($request::input('seo_url'));
                $this->model->uniqueSeoURL($request::input('seo_url'));
            endif;
            if(\PermissionsController::isPackageEnabled('core_open_graph')):
                $open_graph = \OpenGraph::makeOpenGraphData($request);
                $request::merge(['open_graph' => $open_graph]);
            endif;
            if($image = $this->uploadImages($request)):
                $request::merge($image);
            endif;
            $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('solutions.events.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function edit($id) {

        \PermissionsController::allowPermission('solutions_events', 'edit');
        $event = $this->model->findOrFail($id);
        return view('solutions_events_views::events.edit', compact('event'));
    }

    public function update($id) {

        \PermissionsController::allowPermission('solutions_events', 'edit');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $event = $this->model->findOrFail($id);
            if(\PermissionsController::isPackageEnabled('core_seo')):
                $this->model->forbiddenURL($request::input('seo_url'));
                $this->model->uniqueSeoURL($request::input('seo_url'), $id);
            endif;
            if(\PermissionsController::isPackageEnabled('core_open_graph')):
                $open_graph = \OpenGraph::remakeOpenGraphData($event, $request);
                $request::merge(['open_graph' => $open_graph]);
            endif;
            if($images = $this->uploadImages($request, $event)):
                $request::merge($images);
            else:
                $request::merge(['main_image' => $event->main_image, 'announce_image' => $event->announce_image]);
            endif;
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('solutions.events.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function destroy($id) {

        \PermissionsController::allowPermission('solutions_events', 'delete');
        \RequestController::isAJAX()->init();
        $event = $this->model->findOrFail($id);
        if(\PermissionsController::isPackageEnabled('core_open_graph')):
            \OpenGraph::destroyOpenGraphData($event);
        endif;
        $this->deleteImages($event);
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('solutions.events.index'))->json();
    }

    /**************************************************************************************************************/
    private function uploadImages(\Request $request, $event = NULL) {

        if(!is_null($event)):
            $images = ['main_image' => $event->main_image, 'announce_image' => $event->announce_image];
        else:
            $images = ['main_image' => NULL, 'announce_image' => NULL];
        endif;
        if($request::input('main_image_delete') == 1):
            if(!empty($event->main_image) && \Storage::exists($event->main_image)):
                \Storage::delete($event->main_image);
                $event->main_image = NULL;
                $event->save();
            endif;
            $images['main_image'] = NULL;
        endif;
        if($request::input('announce_image_delete') == 1):
            if(!empty($event->announce_image) && \Storage::exists($event->announce_image)):
                \Storage::delete($event->announce_image);
                $event->announce_image = NULL;
                $event->save();
            endif;
            $images['announce_image'] = NULL;
        endif;
        if($request::hasFile('main_image')):
            $fileName = time()."_".rand(1000, 1999).'.'.$request::file('main_image')->getClientOriginalExtension();
            $mainPhotoPath = 'content/events';
            $request::file('main_image')->move('uploads/'.$mainPhotoPath, $fileName);
            $images['main_image'] = add_first_slash($mainPhotoPath.'/'.$fileName);
            if(!empty($event->main_image) && \Storage::exists($event->main_image)):
                \Storage::delete($event->main_image);
                $event->main_image = NULL;
                $event->save();
            endif;
        endif;
        if($request::hasFile('announce_image')):
            $fileName = time()."_".rand(1000, 1999).'.'.$request::file('announce_image')->getClientOriginalExtension();
            $announcePhotoPath = 'content/events/announce';
            $request::file('announce_image')->move('uploads/'.$announcePhotoPath, $fileName);
            $images['announce_image'] = add_first_slash($announcePhotoPath.'/'.$fileName);
            if(!empty($event->announce_image) && \Storage::exists($event->announce_image)):
                \Storage::delete($event->announce_image);
                $event->announce_image = NULL;
                $event->save();
            endif;
        endif;
        return $images;
    }

    private function deleteImages(Event $event) {

        if(!empty($event->main_image) && \Storage::exists($event->main_image)):
            \Storage::delete($event->main_image);
            $event->main_image = NULL;
        endif;
        if(!empty($event->announce_image) && \Storage::exists($event->announce_image)):
            \Storage::delete($event->announce_image);
            $event->announce_image = NULL;
        endif;
        $event->save();
    }
}