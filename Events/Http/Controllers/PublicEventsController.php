<?php
namespace STALKER_CMS\Solutions\Events\Http\Controllers;

use STALKER_CMS\Solutions\Events\Models\Event;

class PublicEventsController extends ModuleController {

    protected $model;

    public function __construct() {

        $this->model = new Event();
    }

    public function showEventPage($url) {

        if(settings(['core_system', 'settings', 'services_mode'])):
            if(view()->exists("home_views::errors.1503")):
                return view("home_views::errors.1503", ['code' => 1503, 'message' => trans('root_lang::codes.1503')]);
            elseif(view()->exists("root_views::errors.1503")):
                return view("root_views::errors.1503", ['code' => 1503, 'message' => trans('root_lang::codes.1503')]);
            endif;
        endif;
        if(is_numeric($url)):
            $page = $this->model->whereId($url)->wherePublication(TRUE)->with('gallery.photos')->first();
        else:
            $page = $this->model->whereSeoUrl($url)->wherePublication(TRUE)->with('gallery.photos')->first();
        endif;
        if($page && view()->exists('home_views::event-single')):
            $page->increment_views();
            return view('home_views::event-single', compact('page'));
        else:
            abort(404);
        endif;
    }

    public function getTags($count = NULL) {

        $eventsTags = [];
        foreach($this->model->where('tags', '!=', '')->whereNotNull('tags')->lists('tags') as $events_tags):
            foreach(explode(',', $events_tags) as $tag):
                if(!isset($eventsTags[$tag])):
                    $eventsTags[$tag] = 1;
                else:
                    $eventsTags[$tag]++;
                endif;
                if(!is_null($count) && count($eventsTags[$tag]) >= $count):
                    break;
                endif;
            endforeach;
        endforeach;
        return $eventsTags;
    }

    public function getEvents($limit = NULL, $offset = NULL) {

        $events = $this->model->whereLocale(\App::getLocale())->wherePublication(TRUE)->orderBy('published_start', 'DESC')->orderBy('created_at', 'DESC');
        if(!is_null($offset)):
            $events = $events->skip($offset);
        endif;
        if(!is_null($limit)):
            $events = $events->take($limit);
        endif;
        return $events->get();
    }
}