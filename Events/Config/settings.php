<?php
return [
    'events' => [
        'title' => ['ru' => 'События', 'en' => 'Events', 'es' => 'Eventos'],
        'options' => [
            ['group_title' => ['ru' => 'Основные', 'en' => 'Main', 'es' => 'Los principales']],
            'first_segment' => [
                'title' => [
                    'ru' => 'Первый сегмент в URL-адресе',
                    'en' => 'The first segment in the URL-address',
                    'es' => 'El primer segmento en la dirección de URL'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => 'events'
            ]
        ]
    ]
];