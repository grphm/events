<?php
return [
    'package' => 'solutions_news',
    'title' => ['ru' => 'События', 'en' => 'Events', 'es' => 'Eventos'],
    'route' => 'solutions.events.index',
    'icon' => 'zmdi zmdi-drink',
    'menu_child' => []
];