<?php
return [
    'package_name' => 'solutions_events',
    'package_title' => ['ru' => 'События', 'en' => 'Events', 'es' => 'Eventos'],
    'package_icon' => 'zmdi zmdi-drink',
    'relations' => ['core_content', 'core_galleries'],
    'package_description' => [
        'ru' => 'Позволяет управлять событиями. Используется модуль "Контент" и "Галереи"',
        'en' => 'It allows you to manage events. Using the module "Content" and "Galleries"',
        'es' => 'Se le permite gestionar eventos. Utilice el módulo de "Contenido" y "Galería"'
    ],
    'version' => [
        'ver' => 1.3,
        'date' => '23.01.2017'
    ]
];